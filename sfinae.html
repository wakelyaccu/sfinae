<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>SFINAE Functionality Is Not Arcane Esoterica</title>
<!-- metadata -->
<meta name="generator" content="Vim" />
<meta name="version" content="S5 1.1" />
<meta name="presdate" content="20130410" />
<meta name="author" content="Jonathan Wakely" />
<!-- configuration parameters -->
<meta name="defaultView" content="slideshow" />
<meta name="controlVis" content="hidden" />
<!-- style sheet links -->
<link rel="stylesheet" href="ui/default/slides.css" type="text/css" media="projection" id="slideProj" />
<link rel="stylesheet" href="ui/default/outline.css" type="text/css" media="screen" id="outlineStyle" />
<link rel="stylesheet" href="ui/default/print.css" type="text/css" media="print" id="slidePrint" />
<link rel="stylesheet" href="ui/default/opera.css" type="text/css" media="projection" id="operaFix" />
<!-- S5 JS -->
<script src="ui/default/slides.js" type="text/javascript"></script>
<style type="text/css" media="screen">
dl.stdref { font-size: smaller }
dl.stdref dt { font-weight: bold; }
pre code span.dull { font-weight: normal; color: silver; }
pre code span.standout { color: blue; }
</style>
<style type="text/css" media="print">
div.slide { page-break-after: always }
</style>
</head>
<body>

<div class="layout">
<div id="controls"><!-- DO NOT EDIT --></div>
<div id="currentSlide"><!-- DO NOT EDIT --></div>
<div id="header"></div>
<div id="footer">
<h1>ACCU 2013</h1>
<h2>SFINAE Functionality Is Not Arcane Esoterica</h2>
</div>

</div>


<div class="presentation">

<div class="slide">
<h1>SFINAE Functionality Is Not Arcane Esoterica</h1>
<h2>(Substitution Failure Is Not An Error FINAE)</h2>
<h3>Jonathan Wakely</h3>
</div>


<div class="slide">
<h1>Sfin-wut?</h1>
<h2>Substitution Failure Is Not An Error</h2>
<ul>
<li>Term coined by David Vandevoorde for the <cite>C++ Templates: The Complete Guide</cite> book with Nicolai Josuttis</li>
<li>Describes an important property of template argument deduction</li>
<li>... which can be used for clever metaprogramming tricks</li>
<li>... and which have become much more powerful in C++11</li>
</ul>
</div>

<div class="slide">
<h1>Template argument deduction</h1>
<dl class='stdref'>
<dt>[temp.deduct]/1</dt>
<dd>When a function template specialization is referenced, all of the template arguments shall have values.
The values can be explicitly specified or, in some cases, be deduced from the use or obtained from default <em>template-arguments</em>.</dd>
</dl>

<p>When you call a function template the compiler <em>deduces</em> the value of each template parameter:
</p>
<pre><code>template&lt;class T&gt;
  T doNothing(T t) { return t; }

int i = doNothing(1);           // T deduced as int
double f = doNothing(1.0f);     // T deduced as float
double f = doNothing&lt;long&gt;(1u); // T deduced as long</code></pre>
</div>

<div class="slide">
<h1>Template argument substitution</h1>
<dl class='stdref'>
<dt>[temp.deduct]/6</dt>
<dd>At certain points in the template argument deduction process it is necessary to take a function type that
makes use of template parameters and replace those template parameters with the corresponding template
arguments. This is done at the beginning of template argument deduction when any explicitly specified template
arguments are substituted into the function type, and again at the end of template argument deduction
when any template arguments that were deduced or obtained from default arguments are substituted.
</dd>
</dl>
<pre><code>template&lt;class T&gt;
  T doNothing(T t) { return t; }

int i = doNothing(1);  // <span class="standout">int</span> doNothing(<span class="standout">int</span>)
</code></pre>
</div>

<div class="slide">
<h1>Type deduction fails</h1>
<dl class='stdref'>
<dt>[temp.deduct]/8</dt>
<dd><strong>If a substitution results in an invalid type or expression, type deduction fails.</strong>
</dd>
</dl>
<pre><code>template&lt;class T&gt;
  <span class="dull">typename</span> T::iterator
  begin(T&amp; t) { return t.begin(); }

int i=0;
int* iter = begin(i);  // substitution failure</code></pre>
<p>Substituting <code>int</code> for <code>T</code> result in an invalid type,<code>int::iterator</code>,
so deduction fails.</p>
</div>


<div class="slide">
<h1>Why is it not an error?</h1>
<p>Substitution has to happen before overload resolution, so the compiler knows which function
template specializations are candidates in the overload set.
</p>
<pre><code>template&lt;class T&gt;
  <span class="dull">typename</span> T::iterator
  begin(T&amp; t) { return t.begin(); }

template&lt;class T, size_t N&gt;
  T*
  begin(T (&amp;array)[N]) { return array; }

int a[2];
int* b = begin(a);</code></pre>
</div>

<div class="slide">
<h1>SFINAE</h1>
<p>When type deduction fails because substitution produces an invalid type
it simply means that function template specialization
<strong>is not a candidate in the overload set</strong>.
</p>
<p>
Substitution failure is not an error, i.e. the program is not ill-formed,
because there might be other overloads
(non-templates, or function template specializations with successfully deduced and substituted arguments)
that can be used.
</p>
<p>If there are no other viable overloads <em>that</em> is still an error as normal,
only substitution failures during type deduction are not "hard errors".
</p>
<div class="handout">
<blockquote>I made up the term to facilitate the explanation of the associated techniques in "C++ Templates": At the time I thought the standard term "deduction failure" emphasized "failure" too much, and would therefore be confusing ("So you mean this program fails?" "No, no, failure is a good thing here.  It's not really _failure_... well it is, but...").</blockquote>
</div>
</div>

<div class="slide">
<h1>Controlling the overload set</h1>
<p>Anything a C++ compiler has to do for good can also be used for evil.</p>
<p>Usually by Boost.</p>
<p>By intentionally triggering a substitution failure you can control which
function templates are part of the overload set.</p>
<p>Enter <code>boost::enable_if</code>.</p>
</div>

<div class="slide">
<h1><code>boost::enable_if</code></h1>
<p>The implementation is trivial:</p>
<pre><code>template&lt;bool Cond, class T = void&gt;
  struct enable_if_c
  { typedef T type; };

template&lt;class T&gt;
  struct enable_if_c&lt;false, T&gt;
  { };

template&lt;class Cond, class T = void&gt;
  struct enable_if : enable_if_c&lt;Cond::value, T&gt;
  { };</code></pre>
</div>

<div class="slide">
<h1><code>boost::enable_if</code></h1>
<p>This allows you to ensure noone calls your function template with a type
that you can't or don't want to support:</p>
<pre><code>template&lt;class T&gt;
  <span class="dull">typename</span> enable_if&lt;is_floating_point&lt;T&gt;, T&gt;::type
  frobnicate(T t)
  { &hellip; }</code></pre>
<p>The first template argument to <code>enable_if</code> can be any compile-time boolean value,
so any property of a type that you can test directly or via a type trait like
<code>is_floating_point</code>.
</p>
</div>

<div class="slide">
<h1><code>boost::enable_if</code></h1>
<p>The C++03 idiom is to use <code>enable_if</code> on the return type:</p>
<pre><code>template&lt;class T&gt;
  <span class="dull">typename</span> enable_if&lt;is_floating_point&lt;T&gt;, T&gt;::type
  frob(T t)
  { &hellip; }</code></pre>
<p>Or to add a dummy function parameter with a default argument:</p>
<pre><code>template&lt;class T&gt;
  T
  frob(T t,
       <span class="dull">typename</span> enable_if&lt;is_floating_point&lt;T&gt; &gt;::type* = 0)
  { &hellip; }</code></pre>
</div>

<div class="slide">
<h1>Type traits</h1>
<p>SFINAE also makes it possible to define custom type traits, by referring to a possibly-invalid
type in a template argument deduction context.</p>
<pre><code>template&lt;class T&gt;
  true_type   is_iter(<span class="dull">typename</span> T::iterator_category*);
template&lt;class T&gt;
  false_type  is_iter(...);

template&lt;class T&gt;
  struct is_iterator
  {
    static const bool value
      = sizeof(is_iter&lt;T&gt;(0)) == sizeof(true_type);
  };</code></pre>

<div class="handout">
<p>Describe typical C++03 implementations of true_type / false_type.</p>
<p>In practice the <code>is_iter</code> functions would be private static members of the trait class.</p>
</div>
</div>


<div class="slide">
<h1>C++03 Limitations</h1>
<p>In C++03 the reasons that cause type deduction to fail are quite limited,
anything except forming an invalid type or attempting invalid conversions such as
<code>(int*)1</code> is a hard error not a deduction failure.</p>

<p>Access checking is done <em>after</em> type deduction, so your trait might
use SFINAE to find that <code>T::iterator_category</code> does exist, so deduction
succeeds, but then you get an access error because the type is private.
</p>

<div class="handout">
Access checking much bigger issue with Expression SFINAE, can't check if
type is copy-constructible if it has a private copy constructor!
</div>
</div>

<div class="slide">
<h1>C++11 power-up</h1>
<p>C++11 makes a number of changes that make SFINAE far more powerful and easier to use</p>
<ul>
<li><code>std::enable_if</code> and dozens of standard type traits are defined in the
<code>&lt;type_traits&gt;</code> header</li>
<li>"Expression SFINAE" allows arbitrary expressions to be used in argument deduction contexts
<ul>
<li>The <code>decltype</code> keyword makes it much easier to query and work with types</li>
<li>Late-specified return types allow function arguments to be referred to</li>
</ul>
</li>
</ul>

<div class="handout">
enable_if has different signature, no disable_if / lazy / etc.
</div>
</div>

<div class="slide">
<h1>C++11 power-up (continued)</h1>

<ul>
<li>Access checking is part of deduction, so referring to private members causes deduction to fail.</li>
<li>Function templates are allowed to have default template arguments</li>
<li>Alias templates greatly simplify syntax</li>
</ul>

</div>


<div class="slide">
<h1><code>std::enable_if</code></h1>
<p>In contrast to the Boost version, <code>std::enable_if</code> takes a <code>bool</code>
as its first argument, not a "metafunction" with a <code>value</code> member<p>

<ul><li>Equivalent to <code>boost::enable_if_c</code> rather than <code>boost::enable_if</code>.</li></ul>

<p>Standard <code>std::true_type</code> and <code>std::false_type</code> types too.</p>

<ul><li>They have the same size, so test their <code>static const bool value</code> member, not size</li></ul>
</div>

<div class="slide">
<h1>SFINAE for Expressions</h1>
<p>Instead of creating a type trait that tests the property you care about you
and passing it to <code>enable_if</code> you can just write code:</p>
<pre><code>template&lt;class T&gt;
  auto
  begin(T&amp; t) -&gt; decltype(t.begin())
  { return t.begin(); }
</code></pre>
<p><code>decltype</code> is an unevaluated context, like <code>sizeof</code>,
so you can write arbitrary expressions using the function arguments.</p>

<p>Invalid expressions result in substitution failure.</p>
</div>

<div class="slide">
<h1>Default template arguments</h1>

<p>C++11 allows function templates to have default template arguments, which can be very useful when there
is no return type to add <code>enable_if</code> to (i.e. for constructors), when you don't want to or can't
add a default function argument, or just to remove clutter from the function signature:</p>
<pre><code>
class Adaptor {
  template&lt;class T, class Iter = <span class="dull">typename</span> T::iterator&gt;
    Adaptor(const T&amp; t)
    { ... }
    ...
};
</code></pre>
</div>

<div class="slide">
<h1>Example - C++03 Style</h1>
<pre><code style="font:smaller">public:
  // requires T is derived from WidgetBase
  template&lt;class T&gt;
    WidgetDecorator(const Ptr&lt;T&gt;&amp; p,
                    <span class="dull">typename</span> boost::enable_if&lt;
                      boost::is_base_of&lt;WidgetBase, T&gt;
                    &gt;::type* = 0)
    : ptr(p) { }
</code></pre>
</div>

<div class="slide">
<h1>Example - C++03 Style</h1>
<pre><code style="font:smaller">private:
  template&lt;class T,
           bool = boost::is_base_of&lt;WidgetBase, T&gt;::value&gt;
    struct IsDerivedWidget
    { typedef void type; };

  template&lt;class T&gt;
    struct IsDerivedWidget&lt;T, false&gt;
    { };

public:
  // requires T is derived from WidgetBase
  template&lt;class T&gt;
    WidgetDecorator(const Ptr&lt;T&gt;&amp; p,
                    <span class="dull">typename</span> IsDerivedWidget&lt;T&gt;::type* = 0)
    : ptr(p) { }
</code></pre>
</div>


<div class="slide">
<h1>Example - Warming Up</h1>
<pre><code style="font:smaller">private:
  template&lt;class T&gt;
    struct IsDerivedWidget
    : std::enable_if&lt;std::is_base_of&lt;WidgetBase, T&gt;::value&gt;
    { };

public:
  // requires T is derived from WidgetBase
  template&lt;class T,
           class Requires = <span class="dull">typename</span> IsDerivedWidget&lt;T&gt;::type&gt;
    WidgetDecorator(const Ptr&lt;T&gt;&amp; p)
    : ptr(p) { }
</code></pre>
</div>

<div class="slide">
<h1>Example - C++11 Hotness</h1>
<pre><code style="font:smaller">private:
  template&lt;class T&gt;
    using IsDerivedWidget = <span class="dull">typename</span>
      std::enable_if&lt;std::is_base_of&lt;WidgetBase, T&gt;::value&gt;
      ::type;

public:
  // requires T is derived from WidgetBase
  template&lt;class T,
           class Requires = IsDerivedWidget&lt;T&gt;&gt;
    WidgetDecorator(const Ptr&lt;T&gt;&amp; p)
    : ptr(p) { }
</code></pre>
</div>


<div class="slide">
<h1>Example - SFINASH</h1>
<pre><code style="font:smaller">private:
  template&lt;class T, bool B = T::value&gt;
    struct If { typedef void enabled; };

  template&lt;class T&gt;
    struct If&lt;T, false&gt; { };

  template&lt;class T&gt;
    struct IsDerivedWidget : std::is_base_of&lt;WidgetBase, T&gt;
    { };

public:
  template&lt;class T,
           class Requires = typename If&lt;IsDerivedWidget&lt;T&gt;&gt;::enabled&gt;
    WidgetDecorator(const Ptr&lt;T&gt;&amp; p)
    : ptr(p) { }
</code></pre>
</div>




<div class="slide">
<h1>Gotchas</h1>
<ul>
<li>SFINAE only works for function templates, not member functions of class templates</li>
<li><code>enable_if</code> conditions must be disjoint</li>
<li>Substitution failures must be in the "immediate context"</li>
</ul>
</div>

<div class="slide">
<h1>Immediate context</h1>
<pre><code>template&lt;class It&gt;
  <span class="dull">typename</span> std::iterator_traits&lt;It&gt;::reference
  dereference(It it)
  { return *it; }

int dereference(int i) { return i; }

auto x = dereference(1);</code></pre>

<p>Is this code valid?</p>
</div>

<div class="slide">
<h1>Immediate context</h1>
<pre><code>template&lt;class It&gt;
  <span class="dull">typename</span> std::iterator_traits&lt;It&gt;::reference
  dereference(It it)
  { return *it; }

int dereference(int i) { return i; }

auto x = dereference(1);</code></pre>

<p><i>It depends</i></p>

<p>It's not guaranteed to work, and not portable</p>
</div>

<div class="slide">
<h1>Immediate context</h1>
<p>The problem is in the definition of <code>iterator_traits</code>:</p>
<pre><code>namespace std {
  template&lt;class Iter&gt;
    struct iterator_traits {
      typedef Iter::difference_type   difference_type;
      typedef Iter::value_type        value_type;
      typedef Iter::reference         reference;
      typedef Iter::pointer           pointer;
      typedef Iter::iterator_category iterator_category;
    };
  template&lt;class T&gt;
    struct iterator_traits&lt;T*&gt;
    { &hellip; };
}</code></pre>

</div>

<div class="slide">
<h1>Immediate context</h1>
<pre><code>template&lt;class It&gt;
  <span class="dull">typename</span> std::iterator_traits&lt;It&gt;::reference
  dereference(It it)
  { return *it; }

auto x = dereference(1);</code></pre>

<p>To do argument substitution the compiler must first instantiate
<code>std::iterator_traits&lt;int&gt;</code>, then check if it
has a nested <code>reference</code> member.</p>

<p>The instantiation is invalid, because it refers to <code>Iter::reference</code>
in a non-SFINAE context.</p>
</div>

<div class="slide">
<h1>Immediate context</h1>
<p>My mental model is to imagine the compiler adding an explicit instantiation
of the types that will be instantiated by argument deduction:</p>
<pre><code>template struct iterator_traits&lt;int&gt;;

template&lt;class It&gt;
  <span class="dull">typename</span> std::iterator_traits&lt;It&gt;::reference
  dereference(It it)
  { return *it; }

auto x = dereference(1);</code></pre>

<p>When such explicit instantiations would compile any
references to invalid types or expressions in the function template declaration
are in the <i>immediate context</i> and SFINAE applies.</p>

</div>



<div class="slide">
<h1>Slides finishing is not an error</h1>
<p>
The XHTML source for these slides is available from
<a href="http://gitorious.org/wakelyaccu/sfinae">http://gitorious.org/wakelyaccu/sfinae</a>
</p>
</div>


<!--
<div class="slide">
<h1>Styles</h1>
</div>

<div class="slide">
<h1>Concepts-Lite</h1>
</div>





<div class="slide">
<h1></h1>
<div class="handout">
</div>
</div>
-->



</div>

</body>
</html>
